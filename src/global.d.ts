declare type Nullable<T> = T | null | undefined;

declare interface ObjectLiteralType {
    [key: string]: any;
}

declare module '*.scss' {
    const styles: { [key: string]: string };
    export = styles;
}
