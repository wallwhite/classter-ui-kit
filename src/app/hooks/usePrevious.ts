import { useEffect, useRef } from 'react';

function usePrevious<UsePreviousValue>(value: UsePreviousValue): Nullable<UsePreviousValue> {
    const ref = useRef<UsePreviousValue>();

    useEffect(() => {
        ref.current = value;
    }, [value]);

    return ref.current;
}

export default usePrevious;
