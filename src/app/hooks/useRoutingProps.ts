import { useContext } from 'react';
import { RoutingContextValue } from 'app/types';
import { RoutingContext } from 'app/components';

const useRoutingProps = (): RoutingContextValue => useContext(RoutingContext);

export default useRoutingProps;
