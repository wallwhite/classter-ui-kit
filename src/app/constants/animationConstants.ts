import { AnimationTimeProps } from 'app/types';

export const ANIMATION_TIME_SEMI_SHORT = (props: AnimationTimeProps = { isInSeconds: true }): number =>
    props.isInSeconds ? 0.2 : 200;

export const ANIMATION_TIME_NORMAL = (props: AnimationTimeProps = { isInSeconds: true }): number =>
    props.isInSeconds ? 0.3 : 300;

export const ANIMATION_TIME_MEDIUM = (props: AnimationTimeProps = { isInSeconds: true }): number =>
    props.isInSeconds ? 0.5 : 500;
