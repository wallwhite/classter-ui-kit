import * as React from 'react';

type ForwardedRefWrapperProps<PropsType> = JSX.IntrinsicAttributes &
    PropsType & {
        forwardedRef?: React.MutableRefObject<HTMLElement | SVGElement | null>;
        children?: React.ReactNode;
    };

type ForwardedRefType =
    | ((instance: HTMLElement | SVGElement | null) => void)
    | React.MutableRefObject<HTMLElement | SVGElement | null>
    | null;

const withForwardedRef = <PropsType extends ObjectLiteralType>(
    Component: React.ComponentType<ObjectLiteralType>,
): React.ForwardRefExoticComponent<
    React.PropsWithoutRef<PropsType> & React.RefAttributes<HTMLElement | SVGElement>
> => {
    const ForwardedRefWrapper: React.FC<ForwardedRefWrapperProps<PropsType>> = ({ forwardRef, ...props }) => {
        const componentRef = React.useRef(null);

        const currentRef = forwardRef || componentRef;

        return <Component {...props} forwardedRef={currentRef} />;
    };

    const forwardRef: React.ForwardRefRenderFunction<HTMLElement | SVGElement, PropsType> = (
        props: React.PropsWithChildren<JSX.IntrinsicAttributes & PropsType>,
        ref: ForwardedRefType,
    ) => <ForwardedRefWrapper {...props} forwardRef={ref} />;

    const componentWithForwardedRef = React.forwardRef(forwardRef);

    return componentWithForwardedRef;
};

export default withForwardedRef;
