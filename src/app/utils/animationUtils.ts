import gsap from 'gsap';
import { AnimationProps } from 'app/types';

export const morphHeightTo = (
    element: Nullable<HTMLElement>,
    { delay = 0, duration = 0, height = 0, ...restProps }: AnimationProps,
): void => {
    if (!element) return;

    gsap.to(element, {
        duration,
        height,
        delay,
        ...restProps,
    });
};

export const morphHeightFrom = (
    element: Nullable<HTMLElement>,
    { delay = 0, duration = 0, height = 0, ...restProps }: AnimationProps,
): void => {
    if (!element) return;

    gsap.from(element, {
        height,
        delay,
        duration,
        ...restProps,
    });
};

export const fadeIn = (element: Nullable<HTMLElement>, { delay = 0, duration = 0 }: AnimationProps): void => {
    if (!element) return;

    gsap.fromTo(
        element,
        {
            visibility: 'hidden',
            opacity: 0,
        },
        {
            visibility: 'visible',
            opacity: 1,
            delay,
            duration,
        },
    );
};

export const fadeOut = (
    element: Nullable<HTMLElement>,
    { delay = 0, duration = 0, onComplete }: AnimationProps,
): void => {
    if (!element) return;

    gsap.fromTo(
        element,
        {
            visibility: 'visible',
            opacity: 1,
        },
        {
            opacity: 0,
            delay,
            duration,
            onComplete,
        },
    );
};

export const toggleFadeSlide = (
    element: Nullable<HTMLElement>,
    { axis = 'x', duration = 0, delay = 0, pathLength = 30, ...rest }: AnimationProps,
    isEnter: boolean,
): void => {
    if (!element) return;

    const opacityStart = isEnter ? 0 : 1;
    const opacityEnd = isEnter ? 1 : 0;

    gsap.fromTo(
        element,
        {
            opacity: opacityStart,
            [axis]: isEnter ? `${pathLength}px` : 0,
        },
        {
            [axis]: isEnter ? 0 : `${pathLength}px`,
            opacity: opacityEnd,
            duration,
            delay,
            ...rest,
        },
    );
};

export const setCSSSync = (element: HTMLElement | HTMLCollection, properties: ObjectLiteralType): void => {
    if (!element) return;

    gsap.set(element, {
        ...properties,
    });
};

export const resetPrevAnimationsOnElement = (element: Nullable<HTMLElement>): void => {
    if (!element) return;

    gsap.killTweensOf(element);
};
