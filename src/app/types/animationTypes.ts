export interface AnimationProps {
    axis?: 'x' | 'y';
    delay?: number;
    duration?: number;
    height?: number;
    visibility?: string;
    opacity?: number;
    onComplete?: GSAPCallback;
    pathLength?: number;
    ease?: string;
}

export interface AnimationTimeProps {
    isInSeconds: boolean;
}
