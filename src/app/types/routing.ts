import React from 'react';

export interface BaseLinkProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
    to?: string;
    href?: string;
    passHref?: boolean;
}

export interface RoutingContextValue {
    pathProp: 'to' | 'href';
    linkComponent: Nullable<React.ComponentType<BaseLinkProps>>;
}
