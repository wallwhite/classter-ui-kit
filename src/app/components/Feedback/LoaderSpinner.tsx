import React, { RefObject } from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import SpinnerIcon from 'app/components/Icons/SpinnerIcon';
import Icon from 'app/components/DataDisplay/Icon';
import * as styles from './LoaderSpinner.module.scss';

interface LoaderSpinner {
    size?: 's' | 'm' | 'l';
    className?: string;
    iconRef?: RefObject<HTMLSpanElement>;
}

const propTypes = {
    /**  @type 's' | 'm' | 'l' */
    size: PropTypes.oneOf(['s', 'm', 'l']),
    className: PropTypes.string,
    /**  @type RefObject<SVGSVGElement> */
    iconRef: PropTypes.exact({ current: PropTypes.node }),
};

const LoaderSpinner = ({ size = 's', className, iconRef }: LoaderSpinner) => {
    const spinnerIconClassName = cx(styles.spinner, className);

    return (
        <Icon size={size} className={spinnerIconClassName} ref={iconRef}>
            <SpinnerIcon />
        </Icon>
    );
};

LoaderSpinner.propTypes = propTypes;

export default LoaderSpinner;
