import React, { HTMLAttributes, RefObject } from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import { withForwardedRef } from 'app/hoc';
import CircleProgress from './CircleProgress';
import LineProgress from './LineProgress';
import { ProgressProps } from './types';
import * as styles from './ProgressBar.module.scss';

interface ProgressBar extends ProgressProps, HTMLAttributes<HTMLDivElement> {
    type?: 'linear' | 'circle';
    size?: 's' | 'm' | 'l';
    forwardedRef?: RefObject<HTMLDivElement>;
}

const propTypes = {
    /**  @type  'linear' | 'circle' */
    type: PropTypes.oneOf(['linear', 'circle']),
    /**  @type 's' | 'm' | 'l' */
    size: PropTypes.oneOf(['s', 'm', 'l']),
    /**  @type RefObject<HTMLDivElement> */
    ref: PropTypes.exact({ current: PropTypes.node }),
    progress: PropTypes.number.isRequired,
    backgroundColor: PropTypes.string,
    progressColor: PropTypes.string,
    isImmediate: PropTypes.bool,
};

const ProgressBar = ({
    type = 'linear',
    size = 'm',
    progress,
    backgroundColor,
    progressColor = 'currentColor',
    isImmediate,
    forwardedRef,
    ...props
}: ProgressBar) => {
    const progressTypeClassName = styles?.[`progressBarWrapper${upperFirst(type)}`];
    const progressSizeClassName = styles?.[`progressBarWrapper${upperFirst(type)}${upperFirst(size)}`];
    const progressBarClassNames = cx(styles.progressBarWrapper, progressTypeClassName, progressSizeClassName);

    return (
        <div className={progressBarClassNames} ref={forwardedRef} {...props}>
            {type === 'linear' ? (
                <LineProgress
                    isImmediate={isImmediate}
                    progress={progress}
                    backgroundColor={backgroundColor}
                    progressColor={progressColor}
                />
            ) : (
                <CircleProgress progress={progress} backgroundColor={backgroundColor} progressColor={progressColor} />
            )}
        </div>
    );
};

ProgressBar.propTypes = propTypes;

export default withForwardedRef(ProgressBar);
