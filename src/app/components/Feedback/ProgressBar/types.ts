export interface ProgressProps {
    progress?: number;
    backgroundColor?: string;
    progressColor?: string;
    isImmediate?: boolean;
}
