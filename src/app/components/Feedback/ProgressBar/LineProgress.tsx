import React, { useEffect, useRef } from 'react';
import { gsap } from 'gsap';
import { ANIMATION_TIME_NORMAL } from 'app/constants';
import { ProgressProps } from './types';
import * as styles from './ProgressBar.module.scss';

const LineProgress: React.FC<ProgressProps> = ({ progress = 0, backgroundColor, progressColor, isImmediate }) => {
    const progressWrapperRef = useRef<HTMLDivElement>(null);
    const progressIndicatorRef = useRef<HTMLDivElement>(null);

    const indicatorStyles = {
        ...(isImmediate && { width: `${progress}` }),
        ...(progressColor && { backgroundColor: progressColor }),
    };

    const wrapperStyles = {
        ...(backgroundColor && { backgroundColor }),
    };

    const drawAnimated = (progressPercentage: number): void => {
        const { current: progressIndicator } = progressIndicatorRef;

        if (!progressIndicator) return;

        if (progressPercentage === 0) gsap.set(progressIndicator, { width: progressPercentage });
        else gsap.to(progressIndicator, { duration: ANIMATION_TIME_NORMAL(), width: progressPercentage });
    };

    const drawProgressIndicator = (): void => {
        const { current: progressWrapper } = progressWrapperRef;
        const { current: progressIndicator } = progressIndicatorRef;

        if (!progressWrapper || !progressIndicator) return;

        const { width: wrapperWidth } = progressWrapper.getBoundingClientRect();

        const progressPercentage = (wrapperWidth / 100) * progress;

        drawAnimated(progressPercentage);
    };

    useEffect(drawProgressIndicator, [progress]);

    return (
        <div className={styles.progressLinear} style={wrapperStyles} ref={progressWrapperRef}>
            <div className={styles.progressLinearIndicator} style={indicatorStyles} ref={progressIndicatorRef} />
        </div>
    );
};

export default LineProgress;
