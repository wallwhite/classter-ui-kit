import React, { useEffect, useRef } from 'react';
import { gsap } from 'gsap';
import { ANIMATION_TIME_NORMAL } from 'app/constants';
import SvgIcon from 'app/components/Icons/SvgIcon';
import { ProgressProps } from './types';
import * as styles from './ProgressBar.module.scss';

const SVG_SIZE = 188;
const SVG_PATH_CIRCLE_LENGTH = 2 * Math.PI * (SVG_SIZE / 2);

const CircleProgress: React.FC<ProgressProps> = ({ progress = 0, backgroundColor, progressColor, isImmediate }) => {
    const circleProgressRef = useRef<SVGElement>(null);
    const circlePathRef = useRef<SVGPathElement>(null);
    const strokeDashArray = `${isImmediate ? (SVG_PATH_CIRCLE_LENGTH / 100) * progress : 0}  ${SVG_PATH_CIRCLE_LENGTH}`;

    const initAnimation = (): void => {
        const { current: circleProgress } = circleProgressRef;
        const { current: circlePath } = circlePathRef;

        if (!circleProgress || !circlePath) return;

        gsap.set(circleProgress, { opacity: 0 });
        gsap.set(circlePath, { opacity: 0 });
        gsap.to(circleProgress, { duration: ANIMATION_TIME_NORMAL(), opacity: 1 });
        gsap.to(circlePath, {
            duration: ANIMATION_TIME_NORMAL(),
            opacity: 1,
            delay: 0.6,
        });
    };

    const drawProgressCircle = (nextProgress: number): void => {
        const { current: circlePath } = circlePathRef;

        if (nextProgress > 100 || !circlePath) return;

        const pathValue = (SVG_PATH_CIRCLE_LENGTH / 100) * nextProgress;
        const strokeDasharray = `${pathValue}  ${SVG_PATH_CIRCLE_LENGTH}`;

        gsap.to(circlePath, { duration: ANIMATION_TIME_NORMAL(), strokeDasharray, ease: 'circ.out' });
    };

    useEffect(initAnimation, []);

    useEffect(() => {
        drawProgressCircle(progress);
    }, [progress]);

    return (
        <SvgIcon
            className={styles.progressCircle}
            ref={circleProgressRef}
            width={SVG_SIZE}
            height={SVG_SIZE}
            viewBox={`0 0 ${SVG_SIZE} ${SVG_SIZE}`}
            fill="none"
        >
            <g fill="none" fillRule="nonzero">
                <path
                    d="M94 182C142.601 182 182 142.601 182 94C182 45.3989 142.601 6 94 6C45.3989 6 6 45.3989 6 94C6 142.601 45.3989 182 94 182Z"
                    stroke={backgroundColor}
                    strokeWidth="12"
                />
            </g>
            <g fill="none" fillRule="nonzero">
                <path
                    ref={circlePathRef}
                    d="M94 6C142.601 6 182 45.399 182 94C182 142.601 142.601 182 94 182C45.3989 182 6 142.601 6 94C6 45.399 45.3989 6 94 6Z"
                    stroke={progressColor}
                    strokeWidth="12"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeDasharray={strokeDashArray}
                    strokeDashoffset="0"
                />
            </g>
        </SvgIcon>
    );
};

export default CircleProgress;
