// Eslint doesn't understand forwardRef wrapper
/* eslint-disable react/require-default-props */
import React, { forwardRef, HTMLAttributes, useEffect, useRef } from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import { useRoutingProps, usePrevious } from 'app/hooks';
import { toggleFadeSlide } from 'app/utils';
import LoaderSpinner from 'app/components/Feedback/LoaderSpinner';
import * as styles from './Button.module.scss';

interface ButtonProps extends HTMLAttributes<Omit<HTMLButtonElement, 'type' | 'disabled'> | HTMLAnchorElement> {
    theme?: 'primary' | 'secondary' | 'success' | 'danger' | 'invisible';
    type?: 'link' | 'button' | 'submit';
    size?: 's' | 'm' | 'l';
    to?: string;
    isOutline?: boolean;
    isActive?: boolean;
    activeClassName?: string;
    className?: string;
    isDisabled?: boolean;
    isLoading?: boolean;
    ariaLabel?: string;
}

const propTypes = {
    /**  @type  'primary' | 'secondary' | 'success' | 'danger' | 'invisible' */
    theme: PropTypes.oneOf(['primary', 'secondary', 'success', 'danger', 'invisible']),
    /**  @type  'link' | 'button' | 'submit' */
    type: PropTypes.oneOf(['link', 'button', 'submit']),
    /**  @type  's' | 'm' | 'l' */
    size: PropTypes.oneOf(['s', 'm', 'l']),
    to: PropTypes.string,
    className: PropTypes.string,
    isOutline: PropTypes.bool,
    isActive: PropTypes.bool,
    activeClassName: PropTypes.string,
    isDisabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    ariaLabel: PropTypes.string,
};

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
    (
        {
            theme = 'primary',
            type = 'button',
            size = 's',
            isOutline,
            className,
            activeClassName,
            isActive,
            isLoading,
            isDisabled,
            children,
            style,
            to,
            ariaLabel,
            ...props
        },
        ref,
    ) => {
        const { pathProp, linkComponent: Link } = useRoutingProps();
        const spinnerRef = useRef<HTMLDivElement>(null);
        const prevIsLoading = usePrevious(isLoading);

        const themeClassName = styles?.[`buttonTheme${upperFirst(theme)}`];
        const sizeClassName = styles?.[`buttonSize${upperFirst(size)}`];
        const activeButtonClassName = activeClassName || styles.buttonActive;
        const buttonClassNames = cx(styles.button, themeClassName, sizeClassName, className, {
            [styles.buttonDisabled]: isDisabled,
            [styles.buttonOutline]: isOutline,
            [styles.buttonLoading]: isLoading,
            [activeButtonClassName]: isActive,
        });

        const linkPathProp = Link ? pathProp : 'href';

        const commonProps = {
            className: buttonClassNames,
            style,
            'aria-label': ariaLabel,
            title: ariaLabel,
            ...props,
        };

        const linkProps = {
            [linkPathProp]: to ?? '#',
            ...commonProps,
        };

        const buttonProps = {
            ...commonProps,
        };

        const renderButtonBody = () =>
            isLoading ? (
                <span ref={spinnerRef}>
                    <LoaderSpinner size="s" />{' '}
                </span>
            ) : (
                children
            );

        const animateSpinner = () => {
            const { current: spinner } = spinnerRef;

            if (prevIsLoading === null || prevIsLoading === isLoading) return;

            toggleFadeSlide(spinner, { duration: 1.4, pathLength: -30, axis: 'y', ease: 'elastic.out(1, 0.3)' }, true);
        };

        useEffect(animateSpinner, [isLoading]);

        if (isDisabled)
            return (
                <button type="button" disabled className={buttonClassNames} ref={ref} aria-label={ariaLabel}>
                    {children}
                </button>
            );

        if (type === 'link') {
            return Link ? <Link {...linkProps}>{children}</Link> : <a {...linkProps}>{children}</a>;
        }

        switch (type) {
            case 'submit':
                return (
                    <button type="submit" {...buttonProps}>
                        {renderButtonBody()}
                    </button>
                );
            default:
                return (
                    <button type="button" {...buttonProps}>
                        {renderButtonBody()}
                    </button>
                );
        }
    },
);

// @ts-ignore: ts is conflicting with forwardRef propTypes
Button.propTypes = propTypes;

export default Button;
