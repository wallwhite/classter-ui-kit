import React, { RefObject } from 'react';
import PropTypes from 'prop-types';
import { withForwardedRef } from 'app/hoc';

export interface SvgElementProps {
    children?: React.ReactNode;
    className?: string;
    color?: string;
    viewBox?: string;
    width?: number | string;
    height?: number | string;
    titleAccess?: string;
    /** This pops is set via `ref` prop using this component as element. Do not use this prop on your's own  */
    forwardedRef?: RefObject<SVGSVGElement>;
}

const propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    color: PropTypes.string,
    viewBox: PropTypes.string,
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    titleAccess: PropTypes.string,
    /**  @type RefObject<SVGSVGElement> */
    ref: PropTypes.exact({ current: PropTypes.node }),
};

const SvgIcon = ({
    children,
    className = '',
    color = 'inherit',
    width,
    height,
    viewBox,
    titleAccess = '',
    forwardedRef,
    ...other
}: SvgElementProps) => (
    <svg
        className={className}
        width={width}
        height={height}
        viewBox={viewBox}
        focusable="false"
        color={color}
        aria-hidden={titleAccess ? undefined : 'true'}
        role={titleAccess ? 'img' : 'presentation'}
        ref={forwardedRef}
        xmlns="http://www.w3.org/2000/svg"
        {...other}
    >
        {children}
        {titleAccess ? <title>{titleAccess}</title> : null}
    </svg>
);

SvgIcon.propTypes = propTypes;

export default withForwardedRef(SvgIcon);
