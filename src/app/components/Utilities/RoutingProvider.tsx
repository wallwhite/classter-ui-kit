import React, { createContext } from 'react';
import { BaseLinkProps } from 'app/types';

export interface RoutingContextValue {
    pathProp: 'to' | 'href';
    linkComponent: Nullable<React.ComponentType<BaseLinkProps>>;
}

const initialValue: RoutingContextValue = {
    pathProp: 'to',
    linkComponent: null,
};

export const RoutingContext = createContext(initialValue);

export const RoutingProvider: React.FC<RoutingContextValue> = ({ children, pathProp, linkComponent }) => {
    const value: RoutingContextValue = { pathProp, linkComponent };

    return <RoutingContext.Provider value={value}>{children}</RoutingContext.Provider>;
};
