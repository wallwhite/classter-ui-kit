import React, { useMemo } from 'react';
import classNames from 'clsx';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import { useRoutingProps } from 'app/hooks';
import icon from 'assets/images/logo.svg';
import iconMin from 'assets/images/logo-min.svg';
import iconMinMonoPrimary from 'assets/images/logo-min-mono-primary.svg';
import iconMinMonoWhite from 'assets/images/logo-min-mono-white.svg';
import iconMonoDark from 'assets/images/logo-mono-dark.svg';
import iconMonoPrimary from 'assets/images/logo-mono-primary.svg';
import iconMonoWhite from 'assets/images/logo-mono-white.svg';
import iconDark from 'assets/images/logo-white.svg';
import * as styles from './Logo.module.scss';

interface LogoProps {
    to: string;
    className?: string;
    useAnchor?: boolean;
    openLinkInNewTab?: boolean;
    color: 'colored' | 'monochrome';
    theme: 'dark' | 'light' | 'primary';
    size: 's' | 'm' | 'l' | 'xl';
    isMinified?: boolean;
    alt?: string;
}

const propTypes = {
    /** path to redirect on logo click */
    to: PropTypes.string,
    className: PropTypes.string,
    /** this prop used to indicate using html anchor element */
    useAnchor: PropTypes.bool,
    /** @type 'colored' | 'monochrome' */
    color: PropTypes.oneOf(['colored', 'monochrome']),
    /** @type 'dark' | 'light' | 'primary' */
    theme: PropTypes.oneOf(['dark', 'light', 'primary']),
    /** @type 's' | 'm' | 'l' | 'xl' */
    size: PropTypes.oneOf(['s', 'm', 'l', 'xl']),
    /** this prop used to open link in new tab */
    openLinkInNewTab: PropTypes.bool,
    isMinified: PropTypes.bool,
    alt: PropTypes.string,
};

const LOGO_CONFIG = {
    classic: {
        colored: {
            dark: iconDark,
            light: icon,
            primary: null,
            default: iconDark,
        },
        monochrome: {
            dark: iconMonoWhite,
            light: iconMonoDark,
            primary: iconMonoPrimary,
            default: iconMonoDark,
        },
    },
    minified: {
        colored: {
            dark: iconMin,
            light: iconMin,
            primary: null,
            default: iconMin,
        },
        monochrome: {
            dark: iconMinMonoWhite,
            light: iconMinMonoPrimary,
            primary: iconMinMonoPrimary,
            default: iconMinMonoPrimary,
        },
    },
};

const Logo = ({
    to = '/',
    className = '',
    useAnchor = true,
    openLinkInNewTab = false,
    isMinified = false,
    theme = 'light',
    color = 'colored',
    size = 'l',
    alt = '',
}: LogoProps) => {
    const { linkComponent: Link, pathProp } = useRoutingProps();
    const logoSizeClassName = styles?.[`logoSize${upperFirst(size)}` as keyof typeof styles];
    const logoClassNames = classNames(styles.logo, className, {
        [styles.logoMinified]: isMinified,
        [logoSizeClassName]: size,
    });

    const logoIconVariant = useMemo(() => {
        if (isMinified) return LOGO_CONFIG.minified?.[color]?.[theme] ?? LOGO_CONFIG.minified.colored.default;
        return LOGO_CONFIG.classic?.[color]?.[theme] ?? LOGO_CONFIG.minified.colored.default;
    }, [theme, isMinified, color, size]);

    const isAbsoluteLink = useAnchor || !Link;
    const linkPathProps = {
        [pathProp]: to,
    };
    const additionalAttributes = openLinkInNewTab
        ? {
              target: '_blank',
          }
        : {};

    const logoIconElement = <img src={logoIconVariant} alt={alt} />;

    return isAbsoluteLink ? (
        <a className={logoClassNames} {...additionalAttributes} href={to}>
            {logoIconElement}
        </a>
    ) : (
        <Link {...linkPathProps} {...additionalAttributes} className={logoClassNames}>
            {logoIconElement}
        </Link>
    );
};

Logo.propTypes = propTypes;

export default Logo;
