import React, { CSSProperties } from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import * as styles from './Skeleton.module.scss';

interface Skeleton {
    type: 'text' | 'circle' | 'rectangle';
    width?: number | string;
    height?: number | string;
    className?: string;
    style?: CSSProperties;
}

const propTypes = {
    /** @type 'text' | 'circle' | 'rectangle' */
    type: PropTypes.oneOf(['text', 'circle', 'rectangle']).isRequired,
    /** @type number | string */
    width: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    /** @type number | string */
    height: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    className: PropTypes.string,
    /** @type CSSProperties */
    style: PropTypes.objectOf(PropTypes.string),
};

const Skeleton = ({ type, width = '100%', height = '14px', className }: Skeleton) => {
    const skeletonTypeClassName = styles?.[`skeletonType${upperFirst(type)}`];
    const skeletonClassName = cx(styles.skeleton, skeletonTypeClassName, className);

    return <div className={skeletonClassName} style={{ width, height }} />;
};

Skeleton.propTypes = propTypes;

export default Skeleton;
