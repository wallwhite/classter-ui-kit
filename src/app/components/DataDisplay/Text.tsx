import React from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import * as styles from './Text.module.scss';

interface TextProps {
    size?: 'tiny' | 'small' | 'medium';
    className?: string;
    as?: 'span' | 'p' | 'div';
    children?: React.ReactNode;
}

const propTypes = {
    /** @type 'tiny' | 'small' | 'medium' */
    size: PropTypes.oneOf(['tiny', 'small', 'medium']),
    /** @type React.ReactNode */
    children: PropTypes.node,
    className: PropTypes.string,
    /** @type 'span' | 'p' | 'div' */
    as: PropTypes.oneOf(['span', 'p', 'div']),
};

const Text = ({ children, size, className = '', as: Element = 'span' }: TextProps) => {
    const sizeClassName = styles?.[`textSize${size}`];
    const textClassNames = cx(styles.text, className, {
        [sizeClassName]: size,
    });

    return <Element className={textClassNames}>{children}</Element>;
};

Text.propTypes = propTypes;

export default Text;
