/* eslint-disable react/forbid-prop-types */
import React from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import * as styles from './Title.module.scss';

interface TitleProps {
    children?: React.ReactNode;
    className?: string;
    wrapperClassName?: string;
    variant?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
    title?: string;
    style?: React.CSSProperties;
}

const propTypes = {
    /**
     * @type React.ReactNode
     */
    children: PropTypes.node,
    className: PropTypes.string,
    wrapperClassName: PropTypes.string,
    /**
     * @type 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
     */
    variant: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
    title: PropTypes.string,
    /**
     * @type React.CSSProperties
     */
    style: PropTypes.object,
};

const Title = ({
    children = '',
    className = '',
    wrapperClassName,
    variant: Element = 'h1',
    title = '',
    style,
}: TitleProps) => {
    const titleClassNames = cx(styles.title, className);
    const wrapperClassNames = cx(styles.titleWrapper, wrapperClassName);

    return (
        <div className={wrapperClassNames}>
            <Element className={titleClassNames} title={title} style={style}>
                {children}
            </Element>
        </div>
    );
};

Title.propTypes = propTypes;

export default Title;
