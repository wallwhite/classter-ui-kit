import React, { RefObject } from 'react';
import cx from 'clsx';
import PropTypes from 'prop-types';
import { withForwardedRef } from 'app/hoc';
import * as styles from './Icon.module.scss';

interface Icon {
    size?: 's' | 'm' | 'l';
    className?: string;
    children?: React.ReactNode;
    forwardedRef?: RefObject<HTMLSpanElement>;
}

const propTypes = {
    /**  @type 's' | 'm' | 'l' */
    size: PropTypes.oneOf(['s', 'm', 'l']),
    className: PropTypes.string,
    /**  @type React.ReactNode; */
    children: PropTypes.node,
    /**  @type RefObject<HTMLSpanElement> */
    ref: PropTypes.exact({ current: PropTypes.oneOf([PropTypes.node, null]) }),
};

const Icon = ({ size = 'm', className, children, forwardedRef }: Icon) => {
    const iconSizeClassName = styles?.[`iconSize${size?.toUpperCase()}`];
    const iconClassNames = cx(styles.iconWrapper, className, {
        [iconSizeClassName]: size,
    });

    return (
        <span className={iconClassNames} ref={forwardedRef}>
            {children}
        </span>
    );
};

Icon.propTypes = propTypes;

export default withForwardedRef(Icon);
