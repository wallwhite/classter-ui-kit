
<p align="center" style="font-size: 1.2rem;">Ready to use documentation for Classter UI.</p>

```bash
npm install @classter-ui
```

```bash
yarn add @classter-ui
```

## Docs

**See the documentation at [ui.classter.io](https://ui.classter.io)** for more information about using!

## License

Licensed under the MIT License, Copyright © 2022-present Yaroslav Romanenko

