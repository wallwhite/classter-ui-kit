import React from 'react';
import cx from 'clsx';
import * as styles from './FlexGrid.module.scss';

/**
 * @type: direction: 'row' | 'column';
 * @type: align: 'start' | 'end' | 'center';
 * @type: justify: 'start' | 'end' | 'center';
 * @type: styles: CSSProperties;
 */

const FlexGrid = ({ direction = 'row', align = 'start', justify = 'start', style, children }) => {
    // @ts-ignore
    const flexGridClassNames = cx(styles.flexGrid, {
        [styles.flexGridRow]: direction === 'row',
        [styles.flexGridColumn]: direction === 'column',
        [styles.flexGridAlignCenter]: align === 'center',
        [styles.flexGridAlignStart]: align === 'start',
        [styles.flexGridAlignEnd]: align === 'end',
        [styles.flexGridJustifyStart]: justify === 'start',
        [styles.flexGridJustifyEnd]: justify === 'end',
        [styles.flexGridJustifyCenter]: justify === 'center',
    });

    return (
        <div className={flexGridClassNames} style={style}>
            {children}
        </div>
    );
};

export default FlexGrid;
