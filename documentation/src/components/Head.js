import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import { SEO as Seo } from './SEO';

const HeadQuery = graphql`
    query HeadQuery {
        site {
            siteMetadata {
                title
            }
        }
    }
`;

export const Head = ({ title }) => {
    const data = useStaticQuery(HeadQuery);
    return <Seo title={title ? `${title} - ${data.site.siteMetadata.title}` : data.site.siteMetadata.title} />;
};
