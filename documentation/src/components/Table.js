import styled from '@xstyled/styled-components';

export const TableContainer = styled.div`
    overflow-y: auto;
    margin: 3 0 2;
`;

export const Table = styled.table`
    width: 100%;
    text-align: left;
    font-size: 90%;
    border-spacing: 0 4px;

    tr {
        background-color: transparent;

        th {
            color: #3d3d4d;
            text-align: left;
            font-weight: bold;
        }
    }

    td,
    th {
        background-color: #f2f2fa;
        border: solid 1px #d2f2fa;
        border-style: solid none;
        padding: 12px;
        &:first-of-type {
            border-left-style: solid;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
        }
        &:last-child {
            border-right-style: solid;
            border-bottom-right-radius: 5px;
            border-top-right-radius: 5px;
        }
    }

    th {
        color: on-background-light;
        background-color: #eef1f6;
        font-weight: 600;
        z-index: 20;
        position: sticky;
        top: 0;
    }

    td {
        font-size: 85%;
        border-top: 1;
        border-bottom: 1;
        background: #f9f9fb;
        border-color: transparent;
    }
`;
