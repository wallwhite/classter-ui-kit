/* stylelint-disable value-keyword-case */
import React, { useState, useEffect } from 'react';
import copy from 'copy-to-clipboard';
import { DocsButton } from '@docs/components';
import { CopyIcon } from '@docs/components/Icons';
import * as styles from './ColorCard.module.scss';

const ColorCard = ({ color: bg, textColor: color }) => {
    const [isCopied, setIsCopied] = useState(false);

    const copyButtonText = isCopied ? 'Copied' : 'Copy';

    const handleCopyButtonClick = () => {
        copy(bg);
        setIsCopied(true);
    };

    useEffect(() => {
        if (!isCopied) return;
        setTimeout(() => {
            setIsCopied(false);
        }, 3000);
    }, [isCopied]);

    return (
        <div className={styles.colorCard} style={{ background: bg }}>
            <DocsButton variant="transparent" className={styles.copyButton} onClick={handleCopyButtonClick}>
                <CopyIcon />
                {copyButtonText}
            </DocsButton>
            <div style={{ color }}>{bg}</div>
        </div>
    );
};

export default ColorCard;
