import React, { createContext, useContext } from 'react';
import { Table, TableContainer } from '@docs/components';

export const ComponentPropsContext = createContext({ props: [] });

export const ComponentProps = () => {
    const { props } = useContext(ComponentPropsContext);

    return (
        <TableContainer>
            <Table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Default value</th>
                        <th>Required</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {props.map(({ id, name, required, type, defaultValue, description }) => (
                        <tr key={id}>
                            <td>{name}</td>
                            <td>
                                <code className="inline-code">{type?.name}</code>
                            </td>
                            <td>{defaultValue?.value && <code className="inline-code">{defaultValue?.value}</code>}</td>
                            <td>{required ? 'Yes' : 'No'}</td>
                            <td>{description.text}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
        </TableContainer>
    );
};
