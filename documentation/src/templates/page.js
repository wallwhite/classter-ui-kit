import { graphql } from 'gatsby';
import React from 'react';
import MDXRenderer from 'gatsby-plugin-mdx/mdx-renderer';
import { ComponentPropsContext } from '@docs/components';

export const pageQuery = graphql`
    query PagePageQuery($id: String!, $componentName: String) {
        mdx(fields: { id: { eq: $id } }) {
            fields {
                pageType
                title
                editLink
                componentName
            }
            body
            tableOfContents
        }
        componentMetadata(displayName: { eq: $componentName }) {
            displayName
            props {
                id
                name
                required
                defaultValue {
                    computed
                    value
                }
                description {
                    text
                    id
                }
                type {
                    name
                }
            }
        }
    }
`;

const Page = ({ data: { mdx, componentMetadata } }) => (
    <ComponentPropsContext.Provider value={{ props: componentMetadata?.props ?? [] }}>
        <MDXRenderer>{mdx.body}</MDXRenderer>
    </ComponentPropsContext.Provider>
);

export default Page;
