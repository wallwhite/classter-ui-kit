/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');

module.exports = {
    name: 'Classter UI',
    siteUrl: 'https://ui.classter.io',
    description: 'Ready to use documentation for Classter UI.',
    baseDirectory: path.resolve(__dirname, '../'),
    author: 'Yaroslav Romanenko',
    sections: ['Guides', 'Theme', 'Typography', 'Icons', 'Components', 'Docs FAQ'],
    navItems: [{ title: 'Docs', url: '/docs/' }],
    twitterAccount: '/',
    githubRepositoryURL: 'https://gitlab.com/wallwhite/classter-ui-kit',
    docSearch: {
        apiKey: '',
        indexName: '',
    },
};
